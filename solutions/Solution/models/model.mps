<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6dba4cfb-06b4-4883-bfe8-182f60645bc3(model)">
  <persistence version="9" />
  <languages>
    <use id="46803809-20ee-443f-bea9-0bee114b90b3" name="org.campagnelab.metar.edgeR" version="1" />
    <devkit ref="8a3636fa-c6ec-4cb0-bc2a-b7143f2a4937(org.campagnelab.metaR)" />
  </languages>
  <imports>
    <import index="9nc5" ref="r:d1a256e6-591a-459f-809c-7fc9df45e4d5(org.campagnelab.mps.XChart.types.roots)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1224500764161" name="jetbrains.mps.baseLanguage.structure.BitwiseAndExpression" flags="nn" index="pVHWs" />
      <concept id="1224500790866" name="jetbrains.mps.baseLanguage.structure.BitwiseOrExpression" flags="nn" index="pVOtf" />
      <concept id="1111509017652" name="jetbrains.mps.baseLanguage.structure.FloatingPointConstant" flags="nn" index="3b6qkQ">
        <property id="1113006610751" name="value" index="$nhwW" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="95951e17-c0d6-47b9-b1b5-42a4ca186fc6" name="org.campagnelab.instantrefresh">
      <concept id="1254484692210402710" name="org.campagnelab.instantrefresh.structure.IAtomic" flags="ng" index="16dhqS">
        <property id="221363389440938160" name="ID" index="1MXi1$" />
      </concept>
    </language>
    <language id="ecc862c9-5ab5-42ef-8703-2039019fb338" name="org.campagnelab.metar.models">
      <concept id="5124039371744206219" name="org.campagnelab.metar.models.structure.BinaryExpression" flags="ng" index="10Wucy">
        <child id="5124039371744206221" name="right" index="10Wuc$" />
        <child id="5124039371744206220" name="left" index="10Wuc_" />
      </concept>
      <concept id="5124039371744206222" name="org.campagnelab.metar.models.structure.GroupFormula" flags="ng" index="10WucB">
        <child id="5124039371744206223" name="groupExpression" index="10WucA" />
      </concept>
      <concept id="5124039371744206225" name="org.campagnelab.metar.models.structure.GroupUsageRef" flags="ng" index="10WucS">
        <reference id="5124039371744206226" name="groupUsage" index="10WucV" />
      </concept>
      <concept id="5124039371744206229" name="org.campagnelab.metar.models.structure.Plus" flags="ng" index="10WucW" />
      <concept id="5124039371744206228" name="org.campagnelab.metar.models.structure.NoIntercept" flags="ng" index="10WucX" />
      <concept id="5124039371743719809" name="org.campagnelab.metar.models.structure.ContrastsBinaryOperator" flags="ng" index="10Y$WC">
        <child id="5124039371743719811" name="right" index="10Y$WE" />
        <child id="5124039371743719810" name="left" index="10Y$WF" />
      </concept>
      <concept id="5124039371743719808" name="org.campagnelab.metar.models.structure.ContrastMinus" flags="ng" index="10Y$WD" />
      <concept id="5124039371743719813" name="org.campagnelab.metar.models.structure.GroupRef" flags="ng" index="10Y$WG">
        <reference id="5124039371743719814" name="group" index="10Y$WJ" />
      </concept>
      <concept id="5124039371746754735" name="org.campagnelab.metar.models.structure.IStatTest" flags="ng" index="1f2fS6">
        <child id="5124039371746755079" name="modelFormula" index="1f2fMI" />
        <child id="5124039371746755432" name="contrasts" index="1f2fR1" />
      </concept>
    </language>
    <language id="46803809-20ee-443f-bea9-0bee114b90b3" name="org.campagnelab.metar.edgeR">
      <concept id="8725455673819557579" name="org.campagnelab.metar.edgeR.structure.EdgeRTest" flags="ng" index="3cumlZ">
        <child id="8725455673819568087" name="countsTable" index="3cupLz" />
        <child id="8725455673819577719" name="destinationTable" index="3curr3" />
        <child id="8725455673820830442" name="dispersionMethod" index="3c_H5u" />
      </concept>
      <concept id="8725455673820802853" name="org.campagnelab.metar.edgeR.structure.TagWiseDispersion" flags="ng" index="3c_Aih" />
    </language>
    <language id="5d6bde84-4ce4-4eb5-a37e-25a5edd55129" name="org.campagnelab.metar.tables">
      <concept id="5703306641526703227" name="org.campagnelab.metar.tables.structure.SelectGroupUsage" flags="ng" index="2tndn">
        <reference id="5703306641526703277" name="usage" index="2tne1" />
      </concept>
      <concept id="5703306641526702642" name="org.campagnelab.metar.tables.structure.HeatmapAnnotations" flags="ng" index="2tnku">
        <property id="6583618001731900686" name="clusterRows" index="2Lhm2Y" />
        <reference id="4173876386489605878" name="idGroup" index="2wXwy3" />
        <child id="5703306641526703109" name="usages" index="2tncD" />
        <child id="6583618001716896682" name="scaling" index="2Mr_oq" />
      </concept>
      <concept id="4451133196879828915" name="org.campagnelab.metar.tables.structure.TableRef" flags="ng" index="afgQW">
        <reference id="4451133196879830023" name="table" index="afgo8" />
      </concept>
      <concept id="3929971219796704543" name="org.campagnelab.metar.tables.structure.OutputFile" flags="ng" index="2jXUOv">
        <property id="3929971219796704769" name="path" index="2jXUS1" />
      </concept>
      <concept id="2133144034279815880" name="org.campagnelab.metar.tables.structure.SelectMultipleGroups" flags="ng" index="2spSBU">
        <child id="2133144034279816044" name="groupSelections" index="2spSxu" />
      </concept>
      <concept id="8031339867720116703" name="org.campagnelab.metar.tables.structure.UsageTypeRef" flags="ng" index="2y_Ijh">
        <reference id="8031339867720116704" name="usage" index="2y_IjI" />
      </concept>
      <concept id="6106414325997850090" name="org.campagnelab.metar.tables.structure.FutureTableCreator" flags="ng" index="2$MLEj">
        <property id="9080041854829670092" name="inputChanged" index="8NYsT" />
      </concept>
      <concept id="8016431400517087678" name="org.campagnelab.metar.tables.structure.UsageType" flags="ng" index="2_mUhs" />
      <concept id="6583618001716898910" name="org.campagnelab.metar.tables.structure.ScaleByRow" flags="ng" index="2Mr_BI" />
      <concept id="7783277237108572280" name="org.campagnelab.metar.tables.structure.FilterWithExpression" flags="ng" index="2Qf$4g">
        <child id="2826789978062873521" name="filter" index="QaakN" />
      </concept>
      <concept id="8962032619593737380" name="org.campagnelab.metar.tables.structure.Analysis" flags="ng" index="S1EQb">
        <property id="2742007948298959018" name="trycatch_enabled" index="2BDq$p" />
        <child id="8962032619593737383" name="statements" index="S1EQ8" />
      </concept>
      <concept id="8962032619593737377" name="org.campagnelab.metar.tables.structure.EmptyLine" flags="ng" index="S1EQe" />
      <concept id="8013388156563171421" name="org.campagnelab.metar.tables.structure.PDF" flags="ng" index="Ss6Tf" />
      <concept id="8013388156563115186" name="org.campagnelab.metar.tables.structure.Render" flags="ng" index="SsgEw">
        <property id="7501650211371753390" name="height" index="165MyL" />
        <property id="7501650211371751513" name="width" index="165MX6" />
        <reference id="8013388156563171415" name="plot" index="Ss6T5" />
        <child id="3929971219796733619" name="path" index="2jX3UN" />
        <child id="8013388156563171423" name="output" index="Ss6Td" />
      </concept>
      <concept id="8962032619582305406" name="org.campagnelab.metar.tables.structure.StatementList" flags="ng" index="ZXjPh">
        <child id="8962032619582305407" name="transformations" index="ZXjPg" />
      </concept>
      <concept id="3105090771424833148" name="org.campagnelab.metar.tables.structure.PlotRef" flags="ng" index="312p7A">
        <reference id="3105090771424833149" name="plot" index="312p7B" />
      </concept>
      <concept id="3105090771424556187" name="org.campagnelab.metar.tables.structure.Multiplot" flags="ng" index="313sG1">
        <property id="3105090771424561488" name="numColumns" index="313rra" />
        <property id="3105090771424561486" name="numRows" index="313rrk" />
        <property id="3105090771427134128" name="preview" index="31lnkE" />
        <child id="3105090771424832493" name="plots" index="312phR" />
        <child id="3105090771426088552" name="destination" index="319mBM" />
      </concept>
      <concept id="3105090771426712763" name="org.campagnelab.metar.tables.structure.PlotRefWithPreview" flags="ng" index="31becx" />
      <concept id="5052319772298911308" name="org.campagnelab.metar.tables.structure.ExpressionWrapper" flags="ng" index="31$ALs">
        <child id="5052319772298911309" name="expression" index="31$ALt" />
      </concept>
      <concept id="8081253674570416584" name="org.campagnelab.metar.tables.structure.ColumnValue" flags="ng" index="3$Gm2I">
        <reference id="8081253674570416585" name="column" index="3$Gm2J" />
      </concept>
      <concept id="8459500803719374384" name="org.campagnelab.metar.tables.structure.Plot" flags="ng" index="1FHg$p">
        <property id="8962032619586498917" name="width" index="ZHjxa" />
        <property id="8962032619586499111" name="height" index="ZHjG8" />
        <property id="4166618652723451261" name="plotId" index="3ZMXzF" />
      </concept>
      <concept id="8459500803719286639" name="org.campagnelab.metar.tables.structure.Heatmap" flags="ng" index="1FHY16">
        <child id="5703306641526697040" name="annotations" index="2thHW" />
        <child id="4451133196879916916" name="table" index="af7lV" />
        <child id="8459500803719374387" name="plot" index="1FHg$q" />
        <child id="8459500803719286733" name="dataSelection" index="1FHY3$" />
      </concept>
      <concept id="3402264987261651661" name="org.campagnelab.metar.tables.structure.ImportTable" flags="ng" index="3MjoWR">
        <reference id="3402264987261692715" name="table" index="3Mj2Vh" />
        <child id="3402264987261651716" name="future" index="3MjoVY" />
      </concept>
      <concept id="3402264987259919045" name="org.campagnelab.metar.tables.structure.FutureTable" flags="ng" index="3MlLWZ">
        <reference id="3402264987259919103" name="table" index="3MlLW5" />
        <child id="4166618652720259019" name="myOwnTable" index="3WeD9t" />
      </concept>
      <concept id="3402264987259164676" name="org.campagnelab.metar.tables.structure.JoinTables" flags="ng" index="3MoTRY">
        <child id="3402264987262235696" name="byKeySelection" index="3MHf7a" />
      </concept>
      <concept id="3402264987259164677" name="org.campagnelab.metar.tables.structure.TableTransformation" flags="ng" index="3MoTRZ">
        <child id="3402264987259853630" name="outputTable" index="3Mq1V4" />
        <child id="3402264987259798258" name="inputTables" index="3Mqss8" />
      </concept>
      <concept id="3402264987258987827" name="org.campagnelab.metar.tables.structure.Table" flags="ng" index="3Mpm39">
        <property id="578023650349875540" name="pathToResolve" index="26T8KA" />
      </concept>
      <concept id="3402264987259789239" name="org.campagnelab.metar.tables.structure.FutureTableRef" flags="ng" index="3MqhDd">
        <reference id="3402264987259798245" name="table" index="3Mqssv" />
      </concept>
      <concept id="3402264987265829888" name="org.campagnelab.metar.tables.structure.ColumnGroupContainer" flags="ng" index="3MzsBU">
        <child id="8031339867719794365" name="usages" index="2yEZeN" />
        <child id="3402264987265829889" name="groups" index="3MzsBV" />
      </concept>
      <concept id="3402264987265829895" name="org.campagnelab.metar.tables.structure.ColumnGroupReference" flags="ng" index="3MzsBX">
        <reference id="3402264987265829896" name="columnGroup" index="3MzsBM" />
      </concept>
      <concept id="3402264987265829883" name="org.campagnelab.metar.tables.structure.ColumnGroup" flags="ng" index="3MzsS1">
        <child id="8031339867720116700" name="usesRefs" index="2y_Iji" />
      </concept>
      <concept id="3402264987265829804" name="org.campagnelab.metar.tables.structure.ColumnAnnotation" flags="ng" index="3MzsTm">
        <child id="3402264987265831176" name="groups" index="3MztjM" />
      </concept>
      <concept id="3402264987266660978" name="org.campagnelab.metar.tables.structure.SelectByGroup" flags="ng" index="3MW7Y8">
        <reference id="3402264987266660979" name="byGroup" index="3MW7Y9" />
      </concept>
      <concept id="4166618652716277483" name="org.campagnelab.metar.tables.structure.SubSetTableRows" flags="ng" index="3WuldX">
        <child id="4451133196880140419" name="table" index="aecac" />
        <child id="4166618652718302640" name="destination" index="3W64wA" />
        <child id="4166618652716281037" name="rowFilter" index="3Wum5r" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="5ec1cd3d-0a50-4049-a8fa-ae768d7baa25" name="org.campagnelab.mps.XChart">
      <concept id="2202909375770430359" name="org.campagnelab.mps.XChart.structure.DataFile" flags="ng" index="31JGnK">
        <property id="2202909375770434162" name="path" index="31JHgl" />
        <child id="2202909375770434164" name="columns" index="31JHgj" />
      </concept>
      <concept id="2202909375770430354" name="org.campagnelab.mps.XChart.structure.DelimitedFile" flags="ng" index="31JGnP">
        <property id="2202909375770898234" name="delimitor" index="31Cu5t" />
      </concept>
      <concept id="2202909375770434159" name="org.campagnelab.mps.XChart.structure.Column" flags="ng" index="31JHg8">
        <reference id="3328299660867197501" name="type" index="1YeEjl" />
      </concept>
    </language>
  </registry>
  <node concept="3Mpm39" id="7l1bKy9WpaX">
    <property role="31Cu5t" value="&#9;" />
    <property role="31JHgl" value="/Users/fac2003/MPSProjects/git/MetaR/data/GSE59364_DC_all.csv" />
    <property role="TrG5h" value="GSE59364_DC_all.csv" />
    <property role="26T8KA" value="/Users/mas2182/Lab/Projects/MPS/2018.1/introtometar_tutorial/data/GSE59364_DC_all.csv" />
    <node concept="31JHg8" id="7l1bKy9Wpi6" role="31JHgj">
      <property role="TrG5h" value="gene" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_9L" resolve="String" />
      <node concept="3MzsTm" id="7l1bKy9WBq_" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WBr$" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WBqL" resolve="ID" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFQ" role="31JHgj">
      <property role="TrG5h" value="mRNA len" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFR" role="31JHgj">
      <property role="TrG5h" value="genomic span" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFS" role="31JHgj">
      <property role="TrG5h" value="DC_normal" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFT" role="31JHgj">
      <property role="TrG5h" value="DC_treated" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFU" role="31JHgj">
      <property role="TrG5h" value="DC0904" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WsIc" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAgH" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPD" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFV" role="31JHgj">
      <property role="TrG5h" value="DC0907" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAgJ" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAhZ" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPO" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFW" role="31JHgj">
      <property role="TrG5h" value="DCLPS0910" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMw" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMv" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAQs" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFX" role="31JHgj">
      <property role="TrG5h" value="DCLPS0913" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMy" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMx" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAQw" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFY" role="31JHgj">
      <property role="TrG5h" value="A_DC" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAM$" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMz" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPR" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpFZ" role="31JHgj">
      <property role="TrG5h" value="A_DC_LPS" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMA" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAM_" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPK" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG0" role="31JHgj">
      <property role="TrG5h" value="B_DC" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMC" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMB" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPT" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG1" role="31JHgj">
      <property role="TrG5h" value="B_DC_LPS" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAME" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMD" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAQA" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG2" role="31JHgj">
      <property role="TrG5h" value="C_DC" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMG" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMF" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPV" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG3" role="31JHgj">
      <property role="TrG5h" value="C_DC_LPS" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMI" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMH" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAQE" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG4" role="31JHgj">
      <property role="TrG5h" value="C2DC" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMK" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMJ" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPX" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG5" role="31JHgj">
      <property role="TrG5h" value="C2DCLPS" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMM" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAML" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAQM" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG6" role="31JHgj">
      <property role="TrG5h" value="C3DC" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMO" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMN" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAPZ" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
        </node>
      </node>
    </node>
    <node concept="31JHg8" id="7l1bKy9WpG7" role="31JHgj">
      <property role="TrG5h" value="C3DCLPS" />
      <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
      <node concept="3MzsTm" id="7l1bKy9WAMQ" role="lGtFl">
        <node concept="3MzsBX" id="7l1bKy9WAMP" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
        </node>
        <node concept="3MzsBX" id="7l1bKy9WAQI" role="3MztjM">
          <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
        </node>
      </node>
    </node>
  </node>
  <node concept="3MzsBU" id="7l1bKy9WpHN">
    <node concept="2_mUhs" id="7l1bKy9WAOO" role="2yEZeN">
      <property role="TrG5h" value="LPS_Treatment" />
    </node>
    <node concept="2_mUhs" id="7l1bKy9WCOP" role="2yEZeN">
      <property role="TrG5h" value="heatmap" />
    </node>
    <node concept="3MzsS1" id="7l1bKy9WBqL" role="3MzsBV">
      <property role="TrG5h" value="ID" />
    </node>
    <node concept="3MzsS1" id="7l1bKy9WAOK" role="3MzsBV">
      <property role="TrG5h" value="LPS=yes" />
      <node concept="2y_Ijh" id="7l1bKy9WAOQ" role="2y_Iji">
        <ref role="2y_IjI" node="7l1bKy9WAOO" resolve="LPS_Treatment" />
      </node>
      <node concept="2y_Ijh" id="7l1bKy9WCOV" role="2y_Iji">
        <ref role="2y_IjI" node="7l1bKy9WCOP" resolve="heatmap" />
      </node>
    </node>
    <node concept="3MzsS1" id="7l1bKy9WAOH" role="3MzsBV">
      <property role="TrG5h" value="LPS=no" />
      <node concept="2y_Ijh" id="7l1bKy9WAOS" role="2y_Iji">
        <ref role="2y_IjI" node="7l1bKy9WAOO" resolve="LPS_Treatment" />
      </node>
      <node concept="2y_Ijh" id="7l1bKy9WCP2" role="2y_Iji">
        <ref role="2y_IjI" node="7l1bKy9WCOP" resolve="heatmap" />
      </node>
    </node>
    <node concept="3MzsS1" id="7l1bKy9WsCW" role="3MzsBV">
      <property role="TrG5h" value="counts" />
    </node>
  </node>
  <node concept="S1EQb" id="7l1bKy9WAQQ">
    <property role="2BDq$p" value="true" />
    <property role="TrG5h" value="diff exp" />
    <node concept="ZXjPh" id="7l1bKy9WAQR" role="S1EQ8">
      <property role="1MXi1$" value="LOJBVKVWQJ" />
      <node concept="3MjoWR" id="7l1bKy9WAR1" role="ZXjPg">
        <property role="1MXi1$" value="UPWGRKBFOE" />
        <ref role="3Mj2Vh" node="7l1bKy9WpaX" resolve="GSE59364_DC_all.csv" />
        <node concept="3MlLWZ" id="7l1bKy9WARd" role="3MjoVY">
          <property role="TrG5h" value="GSE59364_DC_all.csv" />
          <ref role="3MlLW5" node="7l1bKy9WpaX" resolve="GSE59364_DC_all.csv" />
        </node>
      </node>
      <node concept="S1EQe" id="7l1bKy9WARA" role="ZXjPg">
        <property role="1MXi1$" value="YGUSFJGCLB" />
      </node>
      <node concept="3cumlZ" id="7l1bKy9WAVV" role="ZXjPg">
        <property role="8NYsT" value="false" />
        <property role="1MXi1$" value="OBRUMIWVKY" />
        <node concept="3MlLWZ" id="7l1bKy9WAVX" role="3curr3">
          <property role="TrG5h" value="Results" />
          <ref role="3MlLW5" node="7l1bKy9WAVY" resolve="Results" />
          <node concept="3Mpm39" id="7l1bKy9WAVY" role="3WeD9t">
            <property role="31Cu5t" value="&#9;" />
            <property role="TrG5h" value="Results" />
            <node concept="31JHg8" id="7l1bKy9WBti" role="31JHgj">
              <property role="TrG5h" value="logFC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBtj" role="31JHgj">
              <property role="TrG5h" value="logCPM" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBtk" role="31JHgj">
              <property role="TrG5h" value="PValue" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBtl" role="31JHgj">
              <property role="TrG5h" value="FDR" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBtm" role="31JHgj">
              <property role="TrG5h" value="genes" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_9L" resolve="String" />
              <node concept="3MzsTm" id="7l1bKy9WBtn" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBto" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WBqL" resolve="ID" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3c_Aih" id="6cpWLl5On$n" role="3c_H5u" />
        <node concept="afgQW" id="7l1bKy9WAWG" role="3cupLz">
          <ref role="afgo8" node="7l1bKy9WpaX" resolve="GSE59364_DC_all.csv" />
        </node>
        <node concept="10WucB" id="7TVg$8ETFeW" role="1f2fMI">
          <node concept="10WucW" id="7TVg$8ETLx9" role="10WucA">
            <node concept="10WucS" id="7TVg$8ETLxg" role="10Wuc$">
              <ref role="10WucV" node="7l1bKy9WAOO" resolve="LPS_Treatment" />
            </node>
            <node concept="10WucX" id="7TVg$8ETLw0" role="10Wuc_" />
          </node>
        </node>
        <node concept="10Y$WD" id="7TVg$8ETLxm" role="1f2fR1">
          <node concept="10Y$WG" id="7TVg$8ETLxt" role="10Y$WE">
            <ref role="10Y$WJ" node="7l1bKy9WAOK" resolve="LPS=yes" />
          </node>
          <node concept="10Y$WG" id="7TVg$8ETLxj" role="10Y$WF">
            <ref role="10Y$WJ" node="7l1bKy9WAOH" resolve="LPS=no" />
          </node>
        </node>
      </node>
      <node concept="S1EQe" id="7l1bKy9WB$l" role="ZXjPg">
        <property role="1MXi1$" value="ICRJNXHLMG" />
      </node>
      <node concept="3MoTRY" id="7l1bKy9WBv6" role="ZXjPg">
        <property role="8NYsT" value="false" />
        <property role="1MXi1$" value="DLRMFKDXVD" />
        <node concept="3MlLWZ" id="7l1bKy9WBva" role="3Mq1V4">
          <property role="TrG5h" value="joined" />
          <ref role="3MlLW5" node="7l1bKy9WBvb" resolve="joined" />
          <node concept="3Mpm39" id="7l1bKy9WBvb" role="3WeD9t">
            <property role="31Cu5t" value="&#9;" />
            <property role="TrG5h" value="joined" />
            <node concept="31JHg8" id="7l1bKy9WBxC" role="31JHgj">
              <property role="TrG5h" value="PValue" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBwp" role="31JHgj">
              <property role="TrG5h" value="DCLPS0913" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwq" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwr" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBws" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBwi" role="31JHgj">
              <property role="TrG5h" value="DCLPS0910" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwj" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwk" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBwl" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBwb" role="31JHgj">
              <property role="TrG5h" value="DC0907" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwc" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwd" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBwe" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxv" role="31JHgj">
              <property role="TrG5h" value="C3DCLPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBxw" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBxx" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBxy" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBwP" role="31JHgj">
              <property role="TrG5h" value="B_DC_LPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwQ" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwR" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBwS" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxE" role="31JHgj">
              <property role="TrG5h" value="genes" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_9L" resolve="String" />
              <node concept="3MzsTm" id="7l1bKy9WBxF" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBxG" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WBqL" resolve="ID" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBw3" role="31JHgj">
              <property role="TrG5h" value="DC_treated" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxa" role="31JHgj">
              <property role="TrG5h" value="C2DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBxb" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBxc" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBxd" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxB" role="31JHgj">
              <property role="TrG5h" value="logCPM" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBw1" role="31JHgj">
              <property role="TrG5h" value="genomic span" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBwI" role="31JHgj">
              <property role="TrG5h" value="B_DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwJ" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwK" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBwL" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBwB" role="31JHgj">
              <property role="TrG5h" value="A_DC_LPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwC" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwD" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBwE" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxA" role="31JHgj">
              <property role="TrG5h" value="logFC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBw2" role="31JHgj">
              <property role="TrG5h" value="DC_normal" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxD" role="31JHgj">
              <property role="TrG5h" value="FDR" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxo" role="31JHgj">
              <property role="TrG5h" value="C3DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBxp" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBxq" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBxr" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBwW" role="31JHgj">
              <property role="TrG5h" value="C_DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwX" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwY" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBwZ" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBw4" role="31JHgj">
              <property role="TrG5h" value="DC0904" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBw5" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBw6" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBw7" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBw0" role="31JHgj">
              <property role="TrG5h" value="mRNA len" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBxh" role="31JHgj">
              <property role="TrG5h" value="C2DCLPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBxi" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBxj" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBxk" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBvV" role="31JHgj">
              <property role="TrG5h" value="gene" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_9L" resolve="String" />
              <node concept="3MzsTm" id="7l1bKy9WBvW" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBvX" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WBqL" resolve="ID" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBx3" role="31JHgj">
              <property role="TrG5h" value="C_DC_LPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBx4" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBx5" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBx6" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBww" role="31JHgj">
              <property role="TrG5h" value="A_DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBwx" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBwy" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBwz" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3MW7Y8" id="7l1bKy9WBvS" role="3MHf7a">
          <ref role="3MW7Y9" node="7l1bKy9WBqL" resolve="ID" />
        </node>
        <node concept="3MqhDd" id="7l1bKy9WBvK" role="3Mqss8">
          <ref role="3Mqssv" node="7l1bKy9WARd" resolve="GSE59364_DC_all.csv" />
        </node>
        <node concept="3MqhDd" id="7l1bKy9WBvO" role="3Mqss8">
          <ref role="3Mqssv" node="7l1bKy9WAVX" resolve="Results" />
        </node>
      </node>
      <node concept="S1EQe" id="7l1bKy9WB_U" role="ZXjPg">
        <property role="1MXi1$" value="WUSLIYJNTW" />
      </node>
      <node concept="3WuldX" id="7l1bKy9WBD7" role="ZXjPg">
        <property role="8NYsT" value="false" />
        <property role="1MXi1$" value="FEWDGCJIUI" />
        <node concept="3MlLWZ" id="7l1bKy9WBD9" role="3W64wA">
          <property role="TrG5h" value="subset" />
          <ref role="3MlLW5" node="7l1bKy9WBDa" resolve="subset" />
          <node concept="3Mpm39" id="7l1bKy9WBDa" role="3WeD9t">
            <property role="31Cu5t" value="&#9;" />
            <property role="TrG5h" value="subset" />
            <node concept="31JHg8" id="7l1bKy9WBEX" role="31JHgj">
              <property role="TrG5h" value="PValue" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBEY" role="31JHgj">
              <property role="TrG5h" value="DCLPS0913" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBEZ" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBF0" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBF1" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBF5" role="31JHgj">
              <property role="TrG5h" value="DCLPS0910" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBF6" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBF7" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBF8" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFc" role="31JHgj">
              <property role="TrG5h" value="DC0907" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBFd" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBFe" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBFf" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFj" role="31JHgj">
              <property role="TrG5h" value="C3DCLPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBFk" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBFl" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBFm" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFq" role="31JHgj">
              <property role="TrG5h" value="B_DC_LPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBFr" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBFs" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBFt" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFx" role="31JHgj">
              <property role="TrG5h" value="genes" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_9L" resolve="String" />
              <node concept="3MzsTm" id="7l1bKy9WBFy" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBFz" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WBqL" resolve="ID" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFA" role="31JHgj">
              <property role="TrG5h" value="DC_treated" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFB" role="31JHgj">
              <property role="TrG5h" value="C2DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBFC" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBFD" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBFE" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFI" role="31JHgj">
              <property role="TrG5h" value="logCPM" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFJ" role="31JHgj">
              <property role="TrG5h" value="genomic span" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFK" role="31JHgj">
              <property role="TrG5h" value="B_DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBFL" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBFM" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBFN" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFR" role="31JHgj">
              <property role="TrG5h" value="A_DC_LPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBFS" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBFT" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBFU" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFY" role="31JHgj">
              <property role="TrG5h" value="logFC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBFZ" role="31JHgj">
              <property role="TrG5h" value="DC_normal" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBG0" role="31JHgj">
              <property role="TrG5h" value="FDR" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBG1" role="31JHgj">
              <property role="TrG5h" value="C3DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBG2" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBG3" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBG4" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBG8" role="31JHgj">
              <property role="TrG5h" value="C_DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBG9" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBGa" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBGb" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBGf" role="31JHgj">
              <property role="TrG5h" value="DC0904" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBGg" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBGh" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBGi" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBGm" role="31JHgj">
              <property role="TrG5h" value="mRNA len" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
            </node>
            <node concept="31JHg8" id="7l1bKy9WBGn" role="31JHgj">
              <property role="TrG5h" value="C2DCLPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBGo" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBGp" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBGq" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBGu" role="31JHgj">
              <property role="TrG5h" value="gene" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_9L" resolve="String" />
              <node concept="3MzsTm" id="7l1bKy9WBGv" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBGw" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WBqL" resolve="ID" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBGz" role="31JHgj">
              <property role="TrG5h" value="C_DC_LPS" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBG$" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBG_" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBGA" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOK" resolve="LPS=yes" />
                </node>
              </node>
            </node>
            <node concept="31JHg8" id="7l1bKy9WBGE" role="31JHgj">
              <property role="TrG5h" value="A_DC" />
              <ref role="1YeEjl" to="9nc5:1ID5TXdv_7G" resolve="Numeric" />
              <node concept="3MzsTm" id="7l1bKy9WBGF" role="lGtFl">
                <node concept="3MzsBX" id="7l1bKy9WBGG" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WsCW" resolve="counts" />
                </node>
                <node concept="3MzsBX" id="7l1bKy9WBGH" role="3MztjM">
                  <ref role="3MzsBM" node="7l1bKy9WAOH" resolve="LPS=no" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2Qf$4g" id="7l1bKy9WBRR" role="3Wum5r">
          <node concept="31$ALs" id="7l1bKy9WBRT" role="QaakN">
            <node concept="pVHWs" id="7l1bKy9Xad8" role="31$ALt">
              <node concept="1eOMI4" id="7l1bKy9Xahe" role="3uHU7w">
                <node concept="pVOtf" id="7l1bKy9Xd2a" role="1eOMHV">
                  <node concept="3eOSWO" id="7l1bKy9Xd2f" role="3uHU7B">
                    <node concept="3$Gm2I" id="7l1bKy9Xd2g" role="3uHU7B">
                      <ref role="3$Gm2J" node="7l1bKy9WBxA" resolve="logFC" />
                    </node>
                    <node concept="3cmrfG" id="7l1bKy9Xd2h" role="3uHU7w">
                      <property role="3cmrfH" value="2" />
                    </node>
                  </node>
                  <node concept="3eOVzh" id="7l1bKy9Xd2c" role="3uHU7w">
                    <node concept="3$Gm2I" id="7l1bKy9Xd2d" role="3uHU7B">
                      <ref role="3$Gm2J" node="7l1bKy9WBxA" resolve="logFC" />
                    </node>
                    <node concept="3cmrfG" id="7l1bKy9Xd2e" role="3uHU7w">
                      <property role="3cmrfH" value="-2" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOVzh" id="7l1bKy9WCmJ" role="3uHU7B">
                <node concept="3$Gm2I" id="7l1bKy9WCfi" role="3uHU7B">
                  <ref role="3$Gm2J" node="7l1bKy9WBxD" resolve="FDR" />
                </node>
                <node concept="3b6qkQ" id="7l1bKy9WCAi" role="3uHU7w">
                  <property role="$nhwW" value="0.0001" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="afgQW" id="7l1bKy9WBEU" role="aecac">
          <ref role="afgo8" node="7l1bKy9WBvb" resolve="joined" />
        </node>
      </node>
      <node concept="1FHY16" id="7l1bKy9WCLQ" role="ZXjPg">
        <property role="1MXi1$" value="PMMSDDIPIP" />
        <node concept="1FHg$p" id="7l1bKy9WCLS" role="1FHg$q">
          <property role="ZHjxa" value="300" />
          <property role="ZHjG8" value="350" />
          <property role="TrG5h" value="heatmap" />
          <property role="3ZMXzF" value="7" />
        </node>
        <node concept="afgQW" id="7l1bKy9WCOM" role="af7lV">
          <ref role="afgo8" node="7l1bKy9WBDa" resolve="subset" />
        </node>
        <node concept="2tnku" id="7l1bKy9X5ie" role="2thHW">
          <property role="2Lhm2Y" value="true" />
          <ref role="2wXwy3" node="7l1bKy9WBqL" resolve="ID" />
          <node concept="2tndn" id="7l1bKy9X5ig" role="2tncD">
            <ref role="2tne1" node="7l1bKy9WAOO" resolve="LPS_Treatment" />
          </node>
          <node concept="2Mr_BI" id="7l1bKy9X6O0" role="2Mr_oq" />
        </node>
        <node concept="2spSBU" id="7cRhNhC4qQw" role="1FHY3$">
          <node concept="3MW7Y8" id="7cRhNhC4qSM" role="2spSxu">
            <ref role="3MW7Y9" node="7l1bKy9WAOK" resolve="LPS=yes" />
          </node>
          <node concept="3MW7Y8" id="7cRhNhC4qSJ" role="2spSxu">
            <ref role="3MW7Y9" node="7l1bKy9WAOH" resolve="LPS=no" />
          </node>
        </node>
      </node>
      <node concept="SsgEw" id="7l1bKy9X8oM" role="ZXjPg">
        <property role="165MX6" value="6" />
        <property role="165MyL" value="10" />
        <property role="1MXi1$" value="OGNHGDULRW" />
        <ref role="Ss6T5" node="7l1bKy9WCLS" resolve="heatmap" />
        <node concept="Ss6Tf" id="7l1bKy9X8rR" role="Ss6Td" />
        <node concept="2jXUOv" id="7l1bKy9X8oQ" role="2jX3UN">
          <property role="2jXUS1" value="heatmap.pdf" />
        </node>
      </node>
      <node concept="313sG1" id="7l1bKy9XhU7" role="ZXjPg">
        <property role="313rra" value="1" />
        <property role="313rrk" value="1" />
        <property role="31lnkE" value="true" />
        <property role="1MXi1$" value="FWVVQDFGDU" />
        <node concept="1FHg$p" id="7l1bKy9XhU9" role="319mBM">
          <property role="ZHjxa" value="200" />
          <property role="ZHjG8" value="200" />
          <property role="TrG5h" value="preview" />
          <property role="3ZMXzF" value="9" />
        </node>
        <node concept="31becx" id="7l1bKy9Xi6s" role="312phR">
          <ref role="312p7B" node="7l1bKy9WCLS" resolve="heatmap" />
        </node>
      </node>
    </node>
  </node>
</model>

