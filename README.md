# README #

This repository assumes that you have followed the installation instructions for [MetaR](http://metar.campagnelab.org). If not, please follow instructions provided at http://campagnelab.org/software/metar/installation-instructions-mps-3-2/

Note that MetaR can use either a local R installation to execute R scripts, or use docker to run the R script inside a container. If you do not configure docker/Kitematic as shown in installation instructions and configure it (under Preferences/Other Tools/Docker), you should set the path variable R_PATH to the location returned by the R.home() function). Docker configuration is described in the [MetaR documentation](https://play.google.com/store/books/details?id=A9x8BgAAQBAJ) booklet and is recommended to achieve reproducible analyses.

After following these instructions, simply clone the project and open it with MPS. You will then see content similar to [this](http://circles.apps.campagnelab.org/projects/project_details/0c1616e0-dc1e-45f0-81f4-4c5e4ba744e9/modules_project) (best viewed in Safari/Firefox).

## Questions? ##
Please feel free to check the  [user forum](https://groups.google.com/forum/#!forum/metar-users) and ask a question if you cannot find an answer there.